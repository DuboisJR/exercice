#!/bin/bash

# date du jour
backupdate=$(date +%Y-%m-%d)

#répertoire de backup
dirbackup=/home/jr/backup-$backupdate

# création du répertoire de backup
/bin/mkdir $dirbackup

# tar -cjf /destination/fichier.tar.bz2 /source1 /source2 /sourceN
# créé une archive bz2
# sauvegarde de /home
SRCDIR="/var/www/html/wordpress"
 DESTDIR="/home/jr/Backups/"
 FILENAME=wordpress-$(date +%-Y%-m%-d)-$(date +%-T).tgz
 tar --create --gzip --file=$DESTDIR$FILENAME $SRCDIR
# sauvegarde mysql
#/usr/bin/mysqldump --user=doomy --password=935400 wordpress  > $dirbackup/mysqldump-$backupdate.sql.gz
/usr/bin/mysqldump wordpress > /home/jr/wordpress-$backupdate.sql
tar -cf /home/jr/wordpress-$backupdate.tar.gz /home/jr/wordpress-$backupdate.sql
